# Use an appropriate version of the Rust image for the build stage
FROM rust:1.75 AS builder
WORKDIR /usr/src/myapp
USER root
COPY . .
RUN cargo build --release

# Use a smaller base image for the runtime stage
FROM debian:bookworm-slim
WORKDIR /usr/src/myapp

# Copy the compiled binary from the builder stagemm
COPY --from=builder /usr/src/myapp/target/release/mini4 .

# Expose the necessary port
EXPOSE 8080

# Define the command to run the app
CMD ["./mini4"]
