use actix_web::{web, App, HttpServer};

#[actix_web::main]
// say hello when url end with /hi, say goodbye when url is /goodbye, otherwise say silence
async fn main() -> std::io::Result<()> {
  HttpServer::new(|| {
    App::new()
      .route("/hi", web::get().to(|| async { "Hello there! \n  From haoyan ~" }))
      .route("/goodbye", web::get().to(|| async { "Goodbye! \n  From haoyan ~" }))
      .route("/", web::get().to(|| async { "Silence is golden! \n  From haoyan ~" }))
  })
  .bind("0.0.0.0:8080")?
  .run()
  .await
}
