# Personal Mini Web App

## Overview
This project was modified from my mini4 project. The HTTP server is built using Actix-web. 

## Functionality
The server is containerized using Docker and can be run locally. The web app displays a simple message to the user by specifying the path in the URL. The user can access the web app at `http://localhost:8080/` and append the desired path to the URL. For example, `http://localhost:8080/hi` will display the message "Hello, hi!". If the user enters an invalid path, the web app will display a 404 error message. A CI/CD pipeline is implemented using GitHub Actions to automate the testing and deployment process. 
- screenshot of the web app running without a path
![webpage](webpage.png)

## Implementation Steps
1. Begin by creating a new Rust project using `cargo new <YOUR PROJECT NAME>` in the terminal.
2. Write the web application function in `main.rs`, ensuring to import necessary dependencies.
3. Add dependencies in `Cargo.toml`, including `actix-web` for the web framework.
4. Test the web app locally by running `cargo run` in the terminal.
    - screenshot of running locally
    ![local](local.png)
5. Create corresponding files `Dockerfile`. With Docker Desktop running, execute `cargo build`. Then build the Docker image with `docker build -t <YOUR IMAGE NAME> .`.
7. Finally, launch the Docker container with `docker run -d -p 8080:8080 <YOUR IMAGE NAME>`. Utilize Docker Desktop to verify the container status and open the port. Access the web app at `http://localhost:8080/`.
    - screenshot of building the Docker image
    ![docker_build](docker_build.png)
    
    - screenshot of the Docker container running
    ![docker_running](docker_running.png)

    - screenshot of the CI/CD pipeline
    ![ci_cd](ci_cd.png)
    
## Demo Video
- [Demo Video](https://youtu.be/-FfOTO2kqds)